# hotel

#### 介绍

酒店管理系统

#### 软件架构

软件架构说明

### 酒店管理系统功能结构：

分前台用户和后台管理员： 前台用户： 登录、注册，查看房型、预定房型。 后台管理员： 楼层管理： 楼层实体：楼层id、楼层名称、楼层备注。 功能：楼层的增加、编辑、删除功能。 房型管理：
实体：房型id、房型名称、房型价格、房型状态、床位数、可住人数，房型备注。 功能：房型的增加、编辑、删除功能。 房间管理： 实体：房间id、房间编号、所属房型、所属楼层，备注。 功能：增删改查。 客户管理：
实体：客户id、客户登录名、客户登录密码、客户姓名、身份证号、手机号码、联系地址。 功能：增删改查。 预定订单管理： 实体：订单id、用户id、所属房型、入住人姓名、入住人身份证、手机号、预定日期，备注。 功能：增删改查。 入住管理：
实体：id、所属房型、所属房间号、入住人姓名、入住人身份证、手机号、入住日期，离店日期、状态、备注。 统计分析：按照房型统计、统计营业额。待定。

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
